# Contributing

## Contributor License Agreement

Contributions to this repository are subject to the Individual or Corporate Contributor License Agreement, depending on whose behalf a contribution is made:

- By submitting code contributions as an individual to this repository, you agree to the [Individual Contributor License Agreement](https://docs.gitlab.com/ee/legal/individual_contributor_license_agreement.html).

- By submitting code contributions on behalf of a corporation to this repository, you agree to the [Corporate Contributor License Agreement](https://docs.gitlab.com/ee/legal/corporate_contributor_license_agreement.html).

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Code of Conduct

Thank you for your interest in contributing to the GitLab Environment Toolkit! We welcome
all contributions. By participating in this project, you agree to abide by the
[code of conduct](#code-of-conduct).

## Areas for contributions

The GitLab Environment Toolkit touches on a lot of areas including Terraform, Ansible, GitLab, Cloud Providers, Networking and more.

As such, making changes or adding features are typically more involved than one may expect and need to be considered
against [the wider picture that the Toolkit is designed for](#technical-design). This is especially so for new features, which will typically require
scheduled development time from the maintainers to ensure the feature is correct and follows the design.

Based on this we accept contributions to existing features on a case by case basis.

If you are thinking of contributing we're more than happy to discuss this. It's recommended you first check our open issues to see if there's any in the area
you wish to contribute to and ask there or to raise a new issue accordingly for us to talk through the design.

## Technical Design

Before contributing you should read and acknowledge our [Technical Design](TECHNICAL_DESIGN.md) document in full.

## Merge requests

We welcome merge requests with fixes and improvements to GitLab Environment Toolkit code and/or documentation. 
The issues that are specifically suitable for community contributions are listed with the label
[`Accepting Merge Requests` on our issue tracker](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/issues?label_name%5B%5D=Accepting+merge+requests), but you are
free to contribute to any other issue you want.

Please note that if an issue is marked for the current milestone either before
or while you are working on it, a team member may take over the merge request
in order to ensure the work is finished before the release date.

If you want to add a new feature that is not labeled it is best to first create
a feedback issue (if there isn't one already) and leave a comment asking for it
to be marked as `Accepting Merge Requests`.

Merge requests should be opened at [GitLab.com](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/merge_requests).

### Merge request guidelines

Please follow the [Merge Request Workflow](docs/development/merge_request_workflow.md) when working on a merge request.

## Code of conduct

As contributors and maintainers of this project, we pledge to respect all people
who contribute through reporting issues, posting feature requests, updating
documentation, submitting pull requests or patches, and other activities.

We are committed to making participation in this project a harassment-free
experience for everyone, regardless of level of experience, gender, gender
identity and expression, sexual orientation, disability, personal appearance,
body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or reject
comments, commits, code, wiki edits, issues, and other contributions that are
not aligned to this Code of Conduct. Project maintainers who do not follow the
Code of Conduct may be removed from the project team.

This code of conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community.

Instances of abusive, harassing, or otherwise unacceptable behavior can be
reported by emailing contact@gitlab.com.

This Code of Conduct is adapted from the [Contributor Covenant](https://contributor-covenant.org), version 1.1.0,
available at [https://contributor-covenant.org/version/1/1/0/](https://contributor-covenant.org/version/1/1/0/).
